{
  outputs = {
    flake-parts,
    home-manager,
    systems,
    ...
  } @ inputs:
    flake-parts.lib.mkFlake {inherit inputs;} {
      systems = import systems;
      flake = {
      };
      perSystem = {
        inputs',
        pkgs,
        ...
      }: {
        formatter = pkgs.alejandra;
        # see https://github.com/nix-community/home-manager/issues/3075
        legacyPackages.homeConfigurations."coder" = home-manager.lib.homeManagerConfiguration {
          inherit pkgs;

          modules = [
            ({
              config,
              pkgs,
              lib,
              ...
            }: let 
              rusmux = pkgs.rustPlatform.buildRustPackage rec {
                pname = "rusmux";
                version = "0.6.1";
                src = pkgs.fetchFromGitHub {
                  owner = "MeirKriheli";
                  repo = "rusmux";
                  rev = "v${version}";
                  hash = "sha256-8IjMjLeS7mu+SrUz8VkhTBfpnR6NSkHICr4BBv5stW8=";
                };
                cargoHash = "sha256-YBagj7LTUzTK96Fq78fRwktOpNXMMCwbw0/xTOFPg6k=";
              };
            in {
              home = {
                username = "coder";
                homeDirectory = "/home/coder";
                stateVersion = "23.11";
                language.base = "en_US.UTF-8";
                packages = with pkgs; [
                  # rust rewrites of linux tools
                  btop
                  fd
                  eza
                  procs
                  sd
                  dust
                  ripgrep
                  tokei
                  bandwhich
                  delta
                  bat

                  superfile

                  # my vim config
                  inputs'.tfvim.packages.default

                  # needed for tmux stuff
                  gnugrep
                  ps # vim-tmux-navigator
                  sysstat
                  gawk
                  gnused # cpu stats

                  rusmux
                  sesh
                  # NOTE: needed for compatibility as sesh hardcodes "tmuxinator list -n"
                  (pkgs.writeShellScriptBin "tmuxinator" ''
                    if [[ "$1" == "list" ]]; then
                      echo "extra line for tmuxinator compat";
                      ${rusmux}/bin/rusmux list;
                    elif [[ "$1" == "start" ]]; then
                      ${rusmux}/bin/rusmux $@
                    fi
                  '')

                  superd
                ];
                sessionVariables = {
                  "EDITOR" = "nvim";  # for rusmux
                  "XDG_RUNTIME_DIR" = "/tmp/";
                };
                shellAliases = {
                  sf = "superfile";
                  ls = "eza";
                  ll = "eza -alh";
                  batn = "bat --style=full";
                  bat = "bat --style=plain";
                };
                activation."superd" = let
                  ensureRuntimeDir = "env XDG_RUNTIME_DIR=\${XDG_RUNTIME_DIR:-/run/user/$(id -u)}";
                in
                  lib.hm.dag.entryAfter ["writeBoundary"] /* sh */ ''
                    run ${ensureRuntimeDir} ${pkgs.superd}/bin/superctl reload && echo "Reloaded superd" || echo "superd not running, ignoring..."
                  '';
              };
              programs = {
                home-manager.enable = true;
                direnv = {
                  enable = true;
                  enableZshIntegration = true;
                  nix-direnv.enable = true;
                  config.global.hide_env_diff = true;
                };
                bash = {
                  enable = true;
                  profileExtra = /* sh */ ''
                    export PATH=$PATH:/home/coder/.nix-profile/bin
                    exec zsh
                  '';
                };
                zsh = {
                  enable = true;
                  autosuggestion.enable = true;
                  envExtra = /* sh */ ''
                    export PATH=$PATH:/home/coder/.nix-profile/bin
                  '';
                  initExtra = /* sh */ ''
                    bindkey '^[[1;5C' emacs-forward-word
                    bindkey '^[[1;5D' emacs-backward-word
                    bindkey '^[[3~' delete-char

                    # sesh and vi-mode
                    function sesh-list-widget() {
                      exec </dev/tty
                      exec <&1
                      local session
                      session="$(sesh list -i | fzf --ansi)"
                      zle reset-prompt > /dev/null 2>&1 || true
                      [[ -z "$session" ]] && return
                      echo "\nConnecting to $session"
                      sesh connect "$session"
                      zle reset-prompt > /dev/null 2>&1 || true
                    }
                    function btop-widget() {
                      exec </dev/tty
                      exec <&1
                      btop
                    }
                    function zvm_after_init() {
                      zvm_bindkey viins '^R' fzf-history-widget
                      zvm_bindkey viins '^[l' clear-screen
                      zvm_bindkey vicmd '^[l' clear-screen

                      # Sesh: Alt-f
                      zvm_define_widget sesh-list-widget
                      zvm_bindkey viins '^[f' sesh-list-widget
                      zvm_bindkey vicmd '^[f' sesh-list-widget
                      # Btop: Alt-b
                      zvm_define_widget btop-widget
                      zvm_bindkey vicmd '^[b' btop-widget
                      zvm_bindkey viins '^[b' btop-widget
                    }
                  '';
                  sessionVariables = config.home.sessionVariables;
                  plugins = [
                    {
                      name = "vi-mode";
                      src = pkgs.zsh-vi-mode;
                      file = "share/zsh-vi-mode/zsh-vi-mode.plugin.zsh";
                    }
                  ];
                };
                zoxide = {
                  enable = true;
                  enableZshIntegration = true;
                };
                fzf = {
                  enable = true;
                  enableZshIntegration = true;
                };
                starship = {
                  enable = true;
                  enableZshIntegration = true;
                  settings = let 
                    cPrimary = "red";
                    backgroundColor = "black";

                    mkPill = icon: content: "( [](fg:${backgroundColor})[${icon}](fg:${cPrimary} bg:${backgroundColor})[ ${content}](fg:white bg:${backgroundColor})[](fg:${backgroundColor}))";
                  in {
                    format = lib.concatStrings [
                      "[░▒▓](fg:${cPrimary})"
                      "[  ](bg:${cPrimary} fg:${backgroundColor})"
                      "[](fg:${cPrimary})"
                      "$directory"
                      "$git_branch"
                      "$git_metrics"
                      "$direnv"
                      "$nix_shell"
                      "$nodejs"
                      "$rust"
                      "$golang"
                      "\n$character"
                    ];
                    right_format = lib.concatStrings [
                      "$cmd_duration"
                      "$time"
                    ];
                    directory = {
                      format = (mkPill " " "$path");
                      truncation_length = 3;
                      truncate_to_repo = false;
                      truncation_symbol = "…/";
                      substitutions = {
                        "Documents" = "󰈙 ";
                        "Downloads" = " ";
                        "Music" = " ";
                        "Pictures" = " ";
                        "~/repos" = "";
                      };
                    };
                    time = {
                      disabled = false;
                      time_format = "%T"; # Hour:Minute:Second Format
                      format = mkPill " " "$time";
                    };
                    cmd_duration = {
                      format = mkPill "" "$duration";
                    };
                    git_branch = {
                      format = mkPill "" "$branch";
                    };
                    git_metrics = {
                      disabled = false;
                      format = mkPill "" "[+$added](fg:$added_style bg:${backgroundColor}) [-$deleted](fg:$deleted_style bg:${backgroundColor})";
                    };
                    nix_shell = {
                      impure_msg = "";
                      pure_msg = " (pure)";
                      format = mkPill "❄️" "$name$state";
                      heuristic = true;
                    };
                    direnv = {
                      disabled = false;
                      allowed_msg = "✔️";
                      not_allowed_msg = "⚠️";
                      denied_msg = "⚠️";
                      loaded_msg = "✔️";
                      unloaded_msg = "💤";
                      format = mkPill "🌱" "$allowed$loaded";
                    };
                    rust = {
                      format = mkPill "🦀" "$version";
                    };
                  };
                };
                tmux = {
                  enable = true;
                  shell = "${config.programs.zsh.package}/bin/zsh";
                  terminal = "tmux-256color";
                  historyLimit = 100000;
                  clock24 = true;
                  mouse = true;
                  shortcut = "Space";
                  plugins = with pkgs; let 
                    pomodoro = tmuxPlugins.mkTmuxPlugin {
                      pluginName = "tmux-pomodoro-plus";
                      rtpFilePath = "pomodoro.tmux";
                      version = "unstable-2024-11-21";
                      src = fetchFromGitHub {
                        owner = "olimorris";
                        repo = "tmux-pomodoro-plus";
                        rev = "51fb321da594dab5a4baa532b53ea19b628e2822";
                        hash = "sha256-LjhG2+DOAtLwBspOzoI2UDTgbUFWj7vvj6TQXqWw9z0=";
                      };
                    };
                  in [
                    tmuxPlugins.vim-tmux-navigator
                    {
                      plugin = tmuxPlugins.mkTmuxPlugin {
                        pluginName = "tmux2k";
                        rtpFilePath = "2k.tmux";
                        version = "unstable-2024-11-26";
                        src = fetchFromGitHub {
                          owner = "TECHNOFAB11";
                          repo = "tmux2k";
                          rev = "2935a9ea7fc593408017106b78d25a95519600a4";
                          hash = "sha256-YidpBKAguauS+cmSpT2JgTojMlfSnXpGyvHur9LLJ1c=";
                        };
                      };
                      # TODO: cool would be having something like this aswell: https://github.com/MunifTanjim/tmux-mode-indicator/blob/master/mode_indicator.tmux
                      extraConfig = /* tmux */ ''
                        # make border lines thicker
                        set -g pane-border-lines "heavy"

                        set -g @tmux2k-military-time true
                        set -g @tmux2k-theme "duo"
                        set -g @tmux2k-duo-fg "red"
                        set -g @tmux2k-duo-bg "black"
                        set -g @tmux2k-text "black"
                        set -g @tmux2k-light-green "duo_bg" # fixes the flags of the active modifier
                        set -g @tmux2k-cpu-colors "duo_fg text" # fixes the broken colors due to line above
                        set -g @tmux2k-bg-main "default"  # transparent if enabled
                        set -g @tmux2k-window-colors "text duo_fg"  # fix window plugin text being transparent
                        set -g @tmux2k-cwd-colors "text duo_fg"
                        set -g @tmux2k-ram-colors "text duo_fg"
                        set -g @tmux2k-pomodoro-colors "text duo_fg"
                        set -g @tmux2k-current-colors "duo_fg text"

                        set -g @tmux2k-pomodoro-path "${pomodoro}/share/tmux-plugins/tmux-pomodoro-plus"
                        set -g @tmux2k-show-fahrenheit false
                        set -g @tmux2k-right-sep ""
                        set -g @tmux2k-left-sep ""
                        set -g @tmux2k-left-plugins "current cwd git"
                        set -g @tmux2k-right-plugins "pomodoro cpu ram time"  # battery, network not needed here
                      '';
                    }
                    {
                      plugin = tmuxPlugins.resurrect;
                      extraConfig = let 
                        resurrectHook = pkgs.writeShellScriptBin "resurrect-hook" ''
                          ${pkgs.gnused}/bin/sed -i "s| --cmd .*-vim-pack-dir||g; s|/etc/profiles/per-user/$USER/bin/||g; s|/home/$USER/.nix-profile/bin/||g" $1
                        '';
                      in /* tmux */ ''
                        set -g @resurrect-strategy-vim "session"
                        set -g @resurrect-strategy-nvim "session"
                        set -g @resurrect-capture-pane-contents "on"
                        set -g @resurrect-processes ":all:"
                        set -g @resurrect-dir "$HOME/.tmux/resurrect"

                        # fix any nvim installed from Nix
                        set -g @resurrect-hook-post-save-layout "${resurrectHook}/bin/resurrect-hook"
                      '';
                    }
                    {
                      plugin = tmuxPlugins.continuum;
                      extraConfig = /* tmux */ ''
                        # NOTE: with rusmux now auto restore is more annoying than helpful, so disabled for now
                        set -g @continuum-restore "off"
                        set -g @continuum-boot "off"
                        set -g @continuum-save-interval "10"
                      '';
                    }
                    {
                      # TODO: switch to thumbs, but modify it so it works like jump
                      # plugin = tmuxPlugins.tmux-thumbs;
                      plugin = tmuxPlugins.jump;
                      extraConfig = /* tmux */ ''
                        # use s like in vim
                        set -g @jump-key "s"
                      '';
                    }
                    {
                      plugin = pomodoro;
                    }
                  ];
                  extraConfig = let
                    # use custom wrapper which filters out the icon and colors (because we use -i flag with sesh)
                    killSessionWrapper = pkgs.writeShellScriptBin "kill-session-wrapper" ''
                      target=$(echo $1 | awk '{print $NF}')
                      # try the graceful stop with rusmux first, if that fails just kill it
                      rusmux stop $target || tmux kill-session -t $target
                    '';
                  in /* tmux */ ''
                    # for nested tmux sessions, pressing ctrl+space twice will activate the inner
                    bind C-Space send-prefix
                    
                    # set vi-mode
                    set-window-option -g mode-keys vi

                    set -s escape-time 0
                    set -g mode-keys vi
                    set -g detach-on-destroy off
                    set -g exit-empty off
                    set -ga terminal-overrides ',xterm-256color:Tc'
                    set -g focus-events on

                    # add second status bar and make first empty so that there is some padding
                    # NOTE: breaks when reloading the config (both lines show the content of 0)
                    # set -g status 2
                    # set -Fg 'status-format[1]' '#{status-format[0]}'
                    # set -Fg 'status-format[0]' ""
                    
                    # Binds {{{
                    # unbind old and use - and / for splitting
                    unbind '"'
                    unbind %
                    bind - "split-window -v"
                    bind / "split-window -h"

                    # unbind mouse drag end so that selection just stays and nothing is copied instantly
                    unbind-key -T copy-mode-vi MouseDragEnd1Pane
                    bind -T copy-mode-vi 'v' send-keys -X begin-selection
                    bind -T copy-mode-vi 'C-v' send-keys -X rectangle-toggle
                    bind -T copy-mode-vi 'y' send-keys -X copy-selection-and-cancel

                    # reload tmux config with r
                    bind r "source-file ~/.config/tmux/tmux.conf \; display 'tmux configs reloaded'"

                    # sesh popup with Alt-f
                    bind "M-f" run-shell "sesh connect $(
                      sesh list -i | fzf-tmux --ansi -p 55%,60% \
                        --no-sort --border-label ' sesh ' --prompt '⚡  ' \
                        --header '  ^d tmux kill' \
                        --bind 'tab:down,btab:up' \
                        --bind 'ctrl-d:execute(${killSessionWrapper}/bin/kill-session-wrapper {})+change-prompt(⚡  )+reload(sesh list -i)'
                    ) || true"
                    # }}}
                  '';
                };
              };
            })
          ];
        };
      };
    };

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";

    flake-parts.url = "github:hercules-ci/flake-parts";
    systems.url = "github:nix-systems/default";
    home-manager = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    tfvim.url = "gitlab:TECHNOFAB/tf-vim?dir=config";
  };
}
